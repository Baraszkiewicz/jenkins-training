job('dsl_git_gitlab') {
    description 'dsl_git_gitlab'
    scm {
        git {
      		remote {
        		url('https://bitbucket.org/enowe/demoapp.git')
      		}
      		branch('master')
    	}
    }
    triggers {
        scm('*/2 * * * *')
    }
    steps {
        downstreamParameterized {
            trigger('dsl_git2_gitlab') {

                parameters {
                    predefinedProp('COMMIT_SHA', '$GIT_COMMIT')
                }
            }
        }
    }
    wrappers {
        timestamps()
    }
}

job('dsl_git2_gitlab') {
    description 'dsl2_gitlab'
    parameters {
        stringParam('COMMIT_SHA', 'my default stringParam value', 'my description')
    }
    steps {
        shell('echo ${COMMIT_SHA}')
    }
    wrappers {
        timestamps()
    }
}