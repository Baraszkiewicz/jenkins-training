pipelineJob('pipeline-groovy'){
    definition {
        cpsScm {
          scm {
              git {
                remote {
                  name 'origin'
                  url "https://gitlab.com/Baraszkiewicz/jenkins-training.git"
                  branch 'master'
                }
              }
          }
          scriptPath("marcin.jenkinsfile")
        }
        concurrentBuild(false)
      }
}